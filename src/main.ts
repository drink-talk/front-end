import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import ElementUI from "element-ui";
import "@/theme/index.css";
import api from "@/api/api";

Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use(vue => {
  vue.prototype.$api = api;
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount("#app");
