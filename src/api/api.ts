import user from "./user";

export default {
  user,
};

export type ApiType = {
  user: typeof user;
};
