import http from "./http";
import { BaseResponse } from "@/model/BaseResponse";

const user = {
  login(identifier: string, password: string): Promise<boolean> {
    return http
      .post<BaseResponse<boolean>>("/user/auth/login", {
        identifier,
        password,
      })
      .then(res => {
        return res.data.data;
      });
  },
};

export default user;
