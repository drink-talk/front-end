import axios from "axios";

const instance = axios.create({
  baseURL: "http://yh0x13f.cn/tea_talk/api/v1",
  timeout: 10 * 60 * 1000,
});

export default instance;
