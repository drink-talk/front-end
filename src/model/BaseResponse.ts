export interface BaseResponse<T> {
  code: String;
  msg: String;
  data: T;
}
