declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}

declare module "Vue/types/vue" {
  import {ApiType} from "@/api/api";

  interface Vue {
    $api: ApiType
  }
}
