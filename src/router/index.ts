import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import TabBarView from "@/views/TabBarView.vue";
import Home from "@/views/Home.vue";
import Index from "@/views/Index.vue";
import Drinks from "@/views/Drinks.vue";
import Message from "@/views/Message.vue";
import Auth from "@/views/Auth.vue";

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: "/",
    component: TabBarView,
    redirect: "index",
    children: [
      {
        path: "index",
        name: "index",
        component: Index,
      },
      {
        path: "drinks",
        name: "drinks",
        component: Drinks,
      },
      {
        path: "message",
        name: "message",
        component: Message,
      },
      {
        path: "home",
        name: "home",
        component: Home,
      },
    ],
  },
  {
    path: "/auth",
    name: "auth",
    component: Auth,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
